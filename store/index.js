import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);//vue的插件机制

//Vuex.Store 构造器选项
const store = new Vuex.Store({
    state:{//存放状态
        "username":"foo",
        "age":18,
				loginType:0,
				tabbarList:[],
				applyState:0,   //  开题 状态 ：0 为未申请， 1 为已申请
    },
		mutations:{
			setTabbar(){
				/* 1：老师，2: 学生 */
				/* 直接判断state数据 */
				/* 只需要设置三个，第一个为首页，是默认的值 */
				if(store.state.loginType == 2){
					uni.setTabBarItem({
						index: 1,
						text: '任务',
						pagePath:"/pages/student/task/task",
						iconPath: '/static/logo.png',
						selectedIconPath: '/static/logo.png',
						complete: (d) => {
							console.log(d,'store -> setTabBarItem')
						}
					})
					uni.setTabBarItem({
						index: 2,
						text: '提问',
						pagePath:"/pages/student/ask/ask",
						iconPath: '/static/logo.png',
						selectedIconPath: '/static/logo.png',
						complete: (d) => {
							console.log(d,'store -> setTabBarItem')
						}
					})
				}else{
					uni.setTabBarItem({
						index: 0,
						text: '首页',
						pagePath:"/pages/teacher/index/index",
						iconPath: '/static/logo.png',
						selectedIconPath: '/static/logo.png',
						complete: (d) => {
							console.log(d,'store -> setTabBarItem')
						}
					})
					uni.setTabBarItem({
						index: 1,
						text: '列表',
						pagePath:"/pages/teacher/list/list",
						iconPath: '/static/logo.png',
						selectedIconPath: '/static/logo.png',
						complete: (d) => {
							console.log(d,'store -> setTabBarItem')
						}
					})
					uni.setTabBarItem({
						index: 2,
						text: '答疑',
						pagePath:"/pages/teacher/ask/ask",
						iconPath: '/static/logo.png',
						selectedIconPath: '/static/logo.png',
						complete: (d) => {
							console.log(d,'store -> setTabBarItem')
						}
					})
				}
			}
		}
})
export default store